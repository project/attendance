<?php

namespace Drupal\attendance;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining an attendance entity type.
 */
interface AttendanceInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

}
