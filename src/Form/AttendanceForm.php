<?php

namespace Drupal\attendance\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the attendance entity edit forms.
 */
class AttendanceForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $result = parent::save($form, $form_state);

    $entity = $this->getEntity();

    $message_arguments = ['%label' => $entity->toLink()->toString()];
    $logger_arguments = [
      '%label' => $entity->label(),
      'link' => $entity->toLink($this->t('View'))->toString(),
    ];

    switch ($result) {
      case SAVED_NEW:
        $this->messenger()->addStatus($this->t('New attendance %label has been created.', $message_arguments));
        $this->logger('attendance')->notice('Created new attendance %label', $logger_arguments);
        break;

      case SAVED_UPDATED:
        $this->messenger()->addStatus($this->t('The attendance %label has been updated.', $message_arguments));
        $this->logger('attendance')->notice('Updated attendance %label.', $logger_arguments);
        break;
    }

    $form_state->setRedirect('entity.attendance.canonical', ['attendance' => $entity->id()]);

    return $result;
  }

}
