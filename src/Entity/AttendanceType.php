<?php

namespace Drupal\attendance\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Attendance type configuration entity.
 *
 * @ConfigEntityType(
 *   id = "attendance_type",
 *   label = @Translation("Attendance type"),
 *   label_collection = @Translation("Attendance types"),
 *   label_singular = @Translation("attendance type"),
 *   label_plural = @Translation("attendances types"),
 *   label_count = @PluralTranslation(
 *     singular = "@count attendances type",
 *     plural = "@count attendances types",
 *   ),
 *   handlers = {
 *     "form" = {
 *       "add" = "Drupal\attendance\Form\AttendanceTypeForm",
 *       "edit" = "Drupal\attendance\Form\AttendanceTypeForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *     "list_builder" = "Drupal\attendance\AttendanceTypeListBuilder",
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     }
 *   },
 *   admin_permission = "administer attendance types",
 *   bundle_of = "attendance",
 *   config_prefix = "attendance_type",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "add-form" = "/admin/structure/attendance_types/add",
 *     "edit-form" = "/admin/structure/attendance_types/manage/{attendance_type}",
 *     "delete-form" = "/admin/structure/attendance_types/manage/{attendance_type}/delete",
 *     "collection" = "/admin/structure/attendance_types"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "uuid",
 *   }
 * )
 */
class AttendanceType extends ConfigEntityBundleBase {

  /**
   * The machine name of this attendance type.
   *
   * @var string
   */
  protected $id;

  /**
   * The human-readable name of the attendance type.
   *
   * @var string
   */
  protected $label;

}
